package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.util.ArrayList;

public class NauEnemiga {
    private Sprite sprite;
    private int vides=10;
    private Texture texture;
    private boolean eliminada;
    private ArrayList<Sprite> trets;

    public NauEnemiga(Sprite sprite, Texture texture){
        this.sprite = sprite;
        this.texture = texture;
        trets = new ArrayList<Sprite>();
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void treureVida(){
        this.vides--;
        if(this.vides<=0){
            this.eliminada = true;
            sprite.setTexture(texture);
            resetTrets();
        }
    }

    public boolean getEliminada(){
        return eliminada;
    }

    public ArrayList<Sprite> getTrets(){
        return trets;
    }

    public void resetTrets(){
        trets = new ArrayList<Sprite>();
    }
}
