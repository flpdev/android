package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Threads.NauThread;

import com.mygdx.game.Threads.TretsThread;

import java.util.ArrayList;
import java.util.Random;

public class MyGdxGame extends ApplicationAdapter implements InputProcessor, Runnable {

    SpriteBatch batch;
    Texture nauTexture;
    private Sprite nau;
    private static final int MAX_NAUS = 7;
    private int numNaus = 0;
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;
    private Texture tretTextureBo;
    private Texture tretTextureDolent;
    private static int vides = 3;
    private static int score = 0;
    private Texture eliminadaTexture;
    private static boolean jugadorViu = true;
    private ArrayList<Sprite> nausEnemigues;
    private ArrayList<NauEnemiga> nausEnemiguesMostrar = new ArrayList<NauEnemiga>();
    private ArrayList<Sprite> trets;
    private ArrayList<Sprite> tretsDolents;
    private BitmapFont font;
    private int fontCoordX;
    private int fontCoordY;
    private String scoreText;


    @Override
    public void create() {

        Gdx.input.setInputProcessor(this);

        SCREEN_WIDTH = Gdx.graphics.getWidth();
        SCREEN_HEIGHT = Gdx.graphics.getHeight();

        //generacio de les naus enemigues
        nausEnemigues = generarSpritesNaus();
        numNaus++;

        //Spritebatch
        batch = new SpriteBatch();
        font = new BitmapFont();
        scoreText = "SCORE: ";
        fontCoordX = SCREEN_WIDTH / 2;
        fontCoordX = fontCoordX - 100;


        //Textures
        eliminadaTexture = new Texture("nau2e.png");
        tretTextureBo = new Texture("tretBo.png");
        tretTextureDolent = new Texture("tretDolent.png");


        //Nau aliada
        nauTexture = new Texture("nau1.png");
        nau = new Sprite(nauTexture, 0, 0, nauTexture.getWidth(), nauTexture.getHeight());
        nau.setSize(nau.getWidth() * 3, nau.getHeight() * 3);
        nau.setX((SCREEN_WIDTH - nau.getWidth()) / 2);
        nau.setY((SCREEN_HEIGHT / 5f));

        //ArrayList de trets
        trets = new ArrayList<Sprite>();
        tretsDolents = new ArrayList<Sprite>();


        //Threads
        Thread thread = new Thread(this);
        thread.start();

        TretsThread tretThread = new TretsThread(nau, tretTextureBo, trets, nausEnemiguesMostrar, tretTextureDolent, tretsDolents);
        tretThread.start();

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        nau.draw(batch);

        if (vides <= 0) {
            jugadorViu = false;
        }
        if (jugadorViu) {
            for (int i = 0; i < nausEnemiguesMostrar.size(); i++) {
                if (nausEnemiguesMostrar.get(i).getSprite().getY() > 0) {
                    nausEnemiguesMostrar.get(i).getSprite().draw(batch);
                }


                if (nausEnemiguesMostrar.get(i).getTrets().size() > 0) {
                    for (int k = 0; k < nausEnemiguesMostrar.get(i).getTrets().size(); k++) {
                        nausEnemiguesMostrar.get(i).getTrets().get(k).draw(batch);
                    }
                }

            }

            for (int i = 0; i < trets.size(); i++) {
                trets.get(i).draw(batch);
            }
            font.getData().setScale(3);
            font.setColor(Color.YELLOW);
            fontCoordY = (int) nau.getY() / 2;

            font.draw(batch, scoreText + score + " VIDES: " + vides, fontCoordX, fontCoordY);
        } else {
            scoreText = "GAME OVER";
            font.getData().setScale(5);
            font.setColor(Color.YELLOW);
            fontCoordY = SCREEN_HEIGHT / 2;
            fontCoordX = SCREEN_WIDTH / 2;
            font.draw(batch, scoreText, fontCoordX, fontCoordY);
        }

        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
        nauTexture.dispose();
        tretTextureBo.dispose();
        eliminadaTexture.dispose();
    }


    //------- INPUTS -------

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {


        if (jugadorViu) {
            float decimPantalla = SCREEN_WIDTH / 20F;

            if (screenX < (nau.getX() + nau.getWidth() / 2)) {

                if (screenX < (nau.getWidth()) / 2) {
                    if (nau.getX() > decimPantalla) {
                        nau.setX(nau.getX() - decimPantalla);
                    } else {
                        nau.setX(0);
                    }

                } else {
                    nau.setX(nau.getX() - decimPantalla);
                }

            } else {

                if (screenX > SCREEN_WIDTH - (nau.getWidth() / 2)) {
                    if (nau.getX() + nau.getWidth() > SCREEN_WIDTH - decimPantalla) {
                        nau.setX(SCREEN_WIDTH - nau.getWidth());
                    } else {
                        nau.setX(nau.getX() + decimPantalla);
                    }

                } else {
                    nau.setX(nau.getX() + decimPantalla);
                }

            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


    public ArrayList<Sprite> generarSpritesNaus() {

        ArrayList<Sprite> naus = new ArrayList<Sprite>();

        for (int i = 0; i < MAX_NAUS; i++) {
            Texture nau2Texture = new Texture("nau2.png");
            Sprite nau2 = new Sprite(nau2Texture, 0, 0, nau2Texture.getWidth(), nau2Texture.getHeight());
            nau2.setSize(nau2.getWidth() * 3, nau2.getHeight() * 3);

//            if (i == 0) {
//                nau2.setX(randomX((int) nau2.getWidth()));
//                nau2.setY(SCREEN_HEIGHT-nau2.getHeight());
//                naus.add(nau2);
//            } else {
//                do {
//                    nau2.setX(randomX((int) nau2.getWidth()));
//                    nau2.setY(SCREEN_HEIGHT-nau2.getHeight());
//                } while (!esPotColocarNau(nau2.getX(), nau2.getY(), naus));
//
//                naus.add(nau2);
//            }

            nau2.setX(randomX((int) nau2.getWidth()));
            nau2.setY(SCREEN_HEIGHT - nau2.getHeight());
            naus.add(nau2);

        }

        return naus;
    }

    @Override
    public void run() {

        while (jugadorViu) {

            //Afegir nau

            if (numNaus <= nausEnemigues.size() - 1) {
                nausEnemiguesMostrar.add(new NauEnemiga(nausEnemigues.get(numNaus), eliminadaTexture));
                numNaus++;
            }

            for (NauEnemiga s : nausEnemiguesMostrar) {
                NauThread nauThread = new NauThread(s, SCREEN_WIDTH, nau, trets, eliminadaTexture);
                nauThread.start();

            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public boolean esPotColocarNau(float x, float y, ArrayList<Sprite> naus) {

        boolean esPotColocar = false;

        for (int i = 0; i < naus.size(); i++) {


            //AQUEST TROS DE CODI ERA UN INTENT PER DETECTAR COLISIONS ALHORA DE GENERAR LES NAUS
            //l'HE DEIXAT PER QUAN TINGUI TEMPS PODER ARREGLAR-LO, SI VOLS EL POTS ELIMINAR PER CORREGIR


//            if (x < naus.get(i).getX() || x > naus.get(i).getX() + naus.get(i).getWidth()) {
//
//                if(x < naus.get(i).getX()){
//
//                    if(naus.get(i).getX() - x >= naus.get(i).getWidth()){
//                        //Si arriba fins aqui, la X es correcte, ara mirem la Y
//                        if (y < naus.get(i).getY() || y > naus.get(i).getY() + naus.get(i).getHeight()) {
//
//                            if(y < naus.get(i).getY()){
//                                if(naus.get(i).getHeight() - y >= naus.get(i).getHeight()){
//                                    esPotColocar = true;
//                                }else{
//                                    esPotColocar = false;
//                                }
//                            }else{
//                                esPotColocar = true;
//                            }
//
//                        } else {
//                            esPotColocar = false;
//                        }
//                    }else{
//                        esPotColocar = false;
//                    }
//                }else{
//                    if (y < naus.get(i).getY() || y > naus.get(i).getY() + naus.get(i).getHeight()) {
//
//                        if(y < naus.get(i).getY()){
//                            if(naus.get(i).getHeight() - y >= naus.get(i).getHeight()){
//                                esPotColocar = true;
//                            }else{
//                                esPotColocar = false;
//                            }
//                        }else{
//                            esPotColocar = true;
//                        }
//
//                    } else {
//                        esPotColocar = false;
//                    }
//                }
//
//
//            } else {
//                esPotColocar = false;
//            }


            float x1 = naus.get(i).getX();
            float y1 = naus.get(i).getY();
            float nauWidth = naus.get(i).getWidth();
            float nauHeight = naus.get(i).getHeight();

            if (x < x1 - nauWidth) {
                esPotColocar = true;
            } else if (x > x1 + nauWidth) {
                esPotColocar = true;
            } else {
                esPotColocar = false;
            }

            //LA X esta be

            if (esPotColocar) {
                if (y < y1 - nauHeight) {
                    esPotColocar = true;
                } else if (y > y1 + nauHeight) {
                    esPotColocar = true;
                } else {
                    esPotColocar = false;
                }
            }


        }

        return esPotColocar;

    }


    public static void setJugadorViu(Boolean jugadorViu) {
        MyGdxGame.jugadorViu = jugadorViu;
    }

    public int randomX(int max) {
        int num;

        Random r = new Random();
        int low = max;
        int high = SCREEN_WIDTH - max;
        num = r.nextInt(high - low) + low;

        return num;
    }

    public int randomY(int max) {
        int num;

        Random r = new Random();
        int low = SCREEN_HEIGHT / 2;
        int high = SCREEN_HEIGHT - max;
        num = r.nextInt(high - low) + low;

        return num;
    }


    public static void aumentarScore() {
        score++;
    }

    public static void treureVida() {
        vides--;
    }

    public static int getVides() {
        return vides;
    }

}
