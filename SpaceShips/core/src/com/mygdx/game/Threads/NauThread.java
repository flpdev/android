package com.mygdx.game.Threads;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.NauEnemiga;

import java.util.ArrayList;


public class NauThread extends Thread {

    private NauEnemiga nau;
    private float SCREEN_WIDTH;
    private Sprite nauAliada;
    private Texture texture;
    private ArrayList<Sprite> trets;
    private int contadorVides;

    public NauThread(NauEnemiga sprite, float scrrenW, Sprite nauAliada, ArrayList<Sprite> trets, Texture eliminadaTexture) {
        this.nau = sprite;
        this.SCREEN_WIDTH = scrrenW;
        this.nauAliada = nauAliada;
        this.texture = eliminadaTexture;
        this.trets = trets;
    }

    @Override
    public void run() {

        while (nau.getSprite().getY() > 0) {

            nau.getSprite().setY(nau.getSprite().getY() - 10);

            if (nau.getSprite().getY() < nau.getSprite().getHeight()) {
                nau.getSprite().setY(0);
            }

            detectarColisionsAmbNau();


            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    public void detectarColisionsAmbNau() {
        Rectangle rAliada = nauAliada.getBoundingRectangle();
        Rectangle rNau = nau.getSprite().getBoundingRectangle();

        if (!nau.getEliminada()) {
            if (rNau.overlaps(rAliada)) {

                nauAliada.setTexture(texture);


                MyGdxGame.setJugadorViu(false);

            }
        }
    }


}
