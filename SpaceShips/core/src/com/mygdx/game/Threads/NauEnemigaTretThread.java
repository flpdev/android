package com.mygdx.game.Threads;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.NauEnemiga;

import java.util.ArrayList;

public class NauEnemigaTretThread extends Thread {

    private Texture tretTexture;
    private NauEnemiga nau;
    private Sprite tret;
    private ArrayList<Sprite> trets;
    private Sprite nauAliada;

    public NauEnemigaTretThread(Texture tretTexture, NauEnemiga nau, ArrayList<Sprite> trets, Sprite nauAliada) {
        this.tretTexture = tretTexture;
        this.tret =  new Sprite(tretTexture, 0,0, tretTexture.getWidth(), tretTexture.getHeight());
        this.tret.setSize(tret.getWidth()*3, tret.getHeight()*3);
        this.nau = nau;
        this.nau.getTrets().add(tret);
        this.nauAliada = nauAliada;
        this.trets =  trets;
    }

    @Override
    public void run(){
        tret.setX(nau.getSprite().getX() + (nau.getSprite().getWidth()/2));
        tret.setY(nau.getSprite().getY());

        while (!nau.getEliminada()){

            tret.setY(tret.getY() - 20);
            detectarColisions();

            if(tret.getX() < nauAliada.getX()){
                tret.setX(tret.getX() + 5);
            }else{
                tret.setX(tret.getX() - 5);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void detectarColisions(){
        Rectangle rNau = nauAliada.getBoundingRectangle();


        if(nau.getTrets() != null){
            if(nau.getTrets().size() >0){
                for(int i =0; i<nau.getTrets().size(); i++){
                    Rectangle rtret = nau.getTrets().get(i).getBoundingRectangle();
                    if(rNau.overlaps(rtret)){

                        //treure vida
                        if(MyGdxGame.getVides() > 0){
                            MyGdxGame.treureVida();
                        }else{
                            MyGdxGame.setJugadorViu(false);
                        }

                        //eliminar tret
                        nau.getTrets().remove(i);

                    }
                }

            }
        }

    }
}
