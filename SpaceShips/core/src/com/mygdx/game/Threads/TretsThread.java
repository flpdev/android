package com.mygdx.game.Threads;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.NauEnemiga;

import java.util.ArrayList;

public class TretsThread extends Thread{

    private Sprite nau;
    private Texture tretTextureBo;
    private Texture tretTextureDolent;
    private ArrayList<Sprite> trets;
    private ArrayList<Sprite> tretsDolents;
    private ArrayList<NauEnemiga> nausEnemigues;
    private int contador =0;

    public TretsThread(Sprite nau, Texture tretTexture, ArrayList<Sprite> trets, ArrayList<NauEnemiga> nausEnemigues, Texture tretTextureDolent, ArrayList<Sprite> tretsDolents){
        this.nau = nau;
        this.tretTextureBo =  tretTexture;
        this.trets = trets;
        this.nausEnemigues = nausEnemigues;
        this.tretTextureDolent = tretTextureDolent;
        this.tretsDolents = tretsDolents;
    }

    @Override
    public void run(){

        while(true){
            NauAliadaTretThread nauAliadaTretThread = new NauAliadaTretThread(nau, tretTextureBo, trets, nausEnemigues);
            nauAliadaTretThread.start();
            contador++;
            if(contador == 5){
                for(int i=0; i<nausEnemigues.size();i++){
                    if(!nausEnemigues.get(i).getEliminada()){
                        NauEnemigaTretThread nauEnemigaTretThread = new NauEnemigaTretThread(tretTextureDolent,nausEnemigues.get(i),tretsDolents,nau);
                        nauEnemigaTretThread.start();
                    }

                }
                contador = 0;
            }



            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
