package com.mygdx.game.Threads;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.NauEnemiga;

import java.util.ArrayList;

public class NauAliadaTretThread extends Thread {

    private Sprite nau;
    private Texture tretTexture;
    private ArrayList<Sprite> trets;
    private Sprite tret;
    private ArrayList<NauEnemiga> nausEnemigues;

    public NauAliadaTretThread(Sprite nau, Texture tretTexture, ArrayList<Sprite> trets, ArrayList<NauEnemiga> nausEnemigues){
        this.nau = nau;
        this.tretTexture =  tretTexture;
        this.trets = trets;
        this.tret = new Sprite(tretTexture, 0,0, tretTexture.getWidth(), tretTexture.getHeight());
        this.tret.setSize(tret.getWidth()*3, tret.getHeight()*3);
        trets.add(tret);
        this.nausEnemigues = nausEnemigues;
    }


    @Override
    public void run(){
        tret.setX(nau.getX() + (nau.getWidth()/2));
        tret.setY(nau.getY() + nau.getHeight());

        while(true){
            tret.setY(tret.getY() + 20);
            detectarColisions();
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //mirar per cad tret, si colisiona amb alguna nau

    public void detectarColisions(){

        synchronized (trets){
            try{
                if(trets.size() > 0 && nausEnemigues.size() > 0){
                    for(int i = 0; i<trets.size(); i++){
                        for(int k=0; k<nausEnemigues.size(); k++){
                            if(trets.get(i) != null){
                                Rectangle rTret = trets.get(i).getBoundingRectangle();
                                if(!nausEnemigues.get(k).getEliminada()){

                                    Rectangle rNau = nausEnemigues.get(k).getSprite().getBoundingRectangle();

                                    if(rTret.overlaps(rNau)){

                                        nausEnemigues.get(k).treureVida();
                                        trets.remove(i);
                                        MyGdxGame.aumentarScore();
                                    }
                                }
                            }


                        }
                    }
                }
            }catch(Exception e){

            }

        }


    }


}
