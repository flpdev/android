package com.example.esports;

import java.util.ArrayList;

public class Esport {
    protected String nom;
    protected String lliga;
    protected String file;
    protected int numClubs;
    private String img;
    private ArrayList<String> clubs;



    public Esport(String nom, String lliga, String file, int numClubs, String img, ArrayList<String> clubs) {
        this.nom = nom;
        this.lliga = lliga;
        this.file = file;
        this.numClubs = numClubs;
        this.clubs = clubs;
        this.img = img;
    }

    public String getFile() {
        return file;
    }
}
