package com.ferran.verdadoatrevimiento.Adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ferran.verdadoatrevimiento.CustomViews.CustomImageButton;
import com.ferran.verdadoatrevimiento.Interfaces.DeletePlayerListener;
import com.ferran.verdadoatrevimiento.Listeners.Listener;
import com.ferran.verdadoatrevimiento.Pojo.Player;
import com.ferran.verdadoatrevimiento.R;
import com.ferran.verdadoatrevimiento.Utils.DisplayUtils;

import java.util.ArrayList;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.PlayerViewHolder> implements View.OnClickListener, DeletePlayerListener {

    private View.OnClickListener mListener;
    private ArrayList<Player> players;
    private Activity context;

    public PlayerAdapter(final ArrayList<Player> players, Activity context){
        this.players = players;
        this.context = context;
    }

    @NonNull
    @Override
    public PlayerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.player_card, viewGroup, false);
        ViewGroup.LayoutParams layoutParam = v.getLayoutParams();
        layoutParam.height = DisplayUtils.getDisplayDimensions(context).get(DisplayUtils.HEIGHT) / 8;
        layoutParam.width = ViewGroup.LayoutParams.MATCH_PARENT;
        v.setLayoutParams(layoutParam);
        return new PlayerViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull PlayerViewHolder playerViewHolder, int i) {
        playerViewHolder.bindEsport(players.get(i));
        //playerViewHolder.deleteButton.setOnClickListener(new Listener(context).getListener(Listener.DELETE_FLAG));
        playerViewHolder.deleteButton.setDeletePlayerListener(this);

    }

    public void setOnClickListener(View.OnClickListener listener){
        this.mListener = listener;
    }

    public void addPlayer(final Player player){
        players.add(player);
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    @Override
    public void onClick(View v) {
        if(mListener!= null){
            mListener.onClick(v);
        }
    }

    @Override
    public void onClick(int i) {
        players.remove(i);
        notifyDataSetChanged();
    }


    public static class PlayerViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView playerName;
        CustomImageButton deleteButton;

        public PlayerViewHolder(View itemView){
            super(itemView);
            deleteButton = itemView.findViewById(R.id.deletePlayerBtn);
            cardView = (CardView) itemView.findViewById(R.id.player_card);
            playerName = itemView.findViewById(R.id.playerName);
        }

        public void bindEsport(Player player){
            playerName.setText(player.getName());
        }
    }



    public void removePlayer(final int i){
        players.remove(i);
        notifyDataSetChanged();
    }



}
