package com.ferran.verdadoatrevimiento;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ferran.verdadoatrevimiento.Interfaces.UIInterface;

public class MActivity extends AppCompatActivity implements UIInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getSupportActionBar().hide();
    }


    @Override
    public void initUI() {
    }
}
