package com.ferran.verdadoatrevimiento.CustomViews;

import android.content.Context;
import android.util.AttributeSet;

import com.ferran.verdadoatrevimiento.Interfaces.DeletePlayerListener;

public class CustomImageButton extends android.support.v7.widget.AppCompatImageButton {

    private DeletePlayerListener deletePlayerListener;

    public CustomImageButton(Context context) {
        super(context);
    }

    public CustomImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setDeletePlayerListener(DeletePlayerListener deletePlayerListener){
        this.deletePlayerListener = deletePlayerListener;
    }

}
