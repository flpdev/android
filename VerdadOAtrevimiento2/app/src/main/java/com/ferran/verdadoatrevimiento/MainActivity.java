package com.ferran.verdadoatrevimiento;

import android.os.Bundle;
import android.widget.Button;

import com.ferran.verdadoatrevimiento.Interfaces.ActionBar;
import com.ferran.verdadoatrevimiento.Listeners.Listener;

public class MainActivity extends MActivity{

    private Button playButton;
    private Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        playButton.setOnClickListener(new Listener(this).getListener(Listener.PLAY_FLAG));
    }


    @Override
    public void initUI() {
        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_main);

        playButton = findViewById(R.id.newGame);
        addButton = findViewById(R.id.newTruth);
    }
}
