package com.ferran.verdadoatrevimiento.Listeners;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ferran.verdadoatrevimiento.Adapters.PlayerAdapter;
import com.ferran.verdadoatrevimiento.AddPlayersActivity;
import com.ferran.verdadoatrevimiento.Interfaces.DeletePlayerListener;
import com.ferran.verdadoatrevimiento.R;

public class Listener {

    public static final int PLAY_FLAG = 0;
    public static final int DELETE_FLAG = 1;

    private Activity mActivity;

    public Listener(final Activity activity){
        mActivity = activity;
    }

    private void playListener(View v){
        Intent i = new Intent(mActivity.getApplicationContext(), AddPlayersActivity.class);
        mActivity.startActivity(i);
    }


    private void deleteListener(int i){
        RecyclerView r = mActivity.findViewById(R.id.recyclerViewPlayers);
        if(r!= null){
           PlayerAdapter playerAdapter = (PlayerAdapter)r.getAdapter();
           if (playerAdapter != null){
               playerAdapter.removePlayer(i);
           }
        }
    }

    public DeletePlayerListener getDeletePlayerListener(){
        return this::deleteListener;
    }

    public View.OnClickListener getListener(final int flag){
        switch (flag){
            case PLAY_FLAG:
                return this::playListener;
            case DELETE_FLAG:
                //return this::deleteListener;
            default:
                return null;
        }
    }


}
