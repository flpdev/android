package com.ferran.verdadoatrevimiento;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.ferran.verdadoatrevimiento.Adapters.PlayerAdapter;
import com.ferran.verdadoatrevimiento.Pojo.Player;

import java.util.ArrayList;

public class AddPlayersActivity extends MActivity {

    private RecyclerView mRecyclerView;
    private PlayerAdapter mPlayerAdapter;
    private Button btnNewPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUI();
        Player p = new Player("ferran", true);
        Player p2 = new Player("ferran", true);
        Player p3 = new Player("ferran", true);
        ArrayList<Player> players = new ArrayList<>();
        players.add(p);
        players.add(p2);
        players.add(p3);

        mRecyclerView = findViewById(R.id.recyclerViewPlayers);
        mRecyclerView.setHasFixedSize(true);
        PlayerAdapter playerAdapter = new PlayerAdapter(players, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(playerAdapter);
    }

    @Override
    public void initUI() {
        super.initUI();

        setContentView(R.layout.activity_add_players);

        btnNewPlayer = findViewById(R.id.btnNewPlayer);
    }
}
