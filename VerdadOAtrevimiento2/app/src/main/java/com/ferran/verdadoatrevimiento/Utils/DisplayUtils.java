package com.ferran.verdadoatrevimiento.Utils;

import android.app.Activity;
import android.util.DisplayMetrics;

import java.util.HashMap;

public class DisplayUtils {

    public static final String HEIGHT = "HEIGHT";
    public static final String WIDTH = "WIDTH";

    public static HashMap<String, Integer> getDisplayDimensions(Activity context){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        HashMap<String, Integer> map = new HashMap<>();
        map.put(HEIGHT, height);
        map.put(WIDTH, width);

        return map;
    }

}
