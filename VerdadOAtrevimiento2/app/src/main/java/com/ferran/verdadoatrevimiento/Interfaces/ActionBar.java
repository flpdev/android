package com.ferran.verdadoatrevimiento.Interfaces;

import android.app.Activity;
import android.util.Log;
import android.view.WindowManager;

import com.ferran.verdadoatrevimiento.MainActivity;

public interface ActionBar {
    String TAG = ActionBar.class.getName();

    static void hideActionBar(Activity context){

        try{
            MainActivity m = (MainActivity) context;
            //context.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            m.getSupportActionBar().hide();

        } catch (Exception e){
            Log.d(TAG, "hideActionBar() hiding action bar exception");
        }

    }
}
