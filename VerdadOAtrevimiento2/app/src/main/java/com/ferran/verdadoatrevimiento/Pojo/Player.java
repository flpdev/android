package com.ferran.verdadoatrevimiento.Pojo;

public class Player {

    private String name;
    private boolean male;

    public Player(final String name, final boolean male){
        this.name = name;
        this.male = male;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }
}
