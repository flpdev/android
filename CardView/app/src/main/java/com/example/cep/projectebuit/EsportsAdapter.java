package com.example.cep.projectebuit;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class EsportsAdapter extends RecyclerView.Adapter<EsportsAdapter.PlaceViewHolder> implements View.OnClickListener {

    private ArrayList<Esport> esports;
    private View.OnClickListener listener;

    //Constructor
    public EsportsAdapter(ArrayList<Esport> esports){
        this.esports = esports;
    }

    @NonNull
    @Override
    public PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.esports_layout, viewGroup, false);
        itemView.setOnClickListener(this);
        PlaceViewHolder PlaceViewHolder = new PlaceViewHolder(itemView);
        return PlaceViewHolder;
    }

    //Override del metode onClick
    @Override
    public void onClick(View view) {
        if(listener!= null){
            listener.onClick(view);
        }

    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceViewHolder placeViewHolder, int i) {
        placeViewHolder.bindEsport(esports.get(i));
    }

    @Override
    public int getItemCount() {
        return esports.size();
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView txtEsport;
        TextView txtLliga;
        TextView txtNumClubs;

        public PlaceViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imgEsport);
            txtEsport = itemView.findViewById(R.id.nomEsport);
            txtLliga = itemView.findViewById(R.id.lliga);
            txtNumClubs = itemView.findViewById(R.id.numClubs);
        }

        public void bindEsport(Esport esport){
            Bitmap bitmap = BitmapFactory.decodeFile(esport.getImg());
            imageView.setImageBitmap(bitmap);

            txtEsport.setText(esport.getNom());
            txtLliga.setText(esport.getLliga());
            txtNumClubs.setText("Núm de clubs: " + esport.getNumClubs());

        }
    }
}
