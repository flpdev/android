package com.example.cep.projectebuit;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class Esport_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esport_activity);

        final Esport esport = getIntent().getParcelableExtra("esport");

        TextView txtNomEsport = findViewById(R.id.nomEsport);
        TextView txtNomLliga = findViewById(R.id.nomLliga);
        ImageView imgEsport = findViewById(R.id.imgEsport);

        txtNomEsport.setText(esport.getNom());
        txtNomLliga.setText(esport.getLliga());

        Bitmap bitmap = BitmapFactory.decodeFile(esport.getImg());
        imgEsport.setImageBitmap(bitmap);

        GridView gridView = findViewById(R.id.gridView);
        gridView.setAdapter(new ArrayAdapter(this, R.layout.text_layout, esport.getClubs()));

    }
}
