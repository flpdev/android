package com.example.cep.projectebuit;

import android.app.Activity;
import android.os.Environment;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class EsportManager {

    public static final String DIR_SEPAR = File.separator;
    private static final String DIRECTORI_ESPORTS = Environment.getExternalStorageDirectory() + DIR_SEPAR + "Esports";
    private static final String DIRECTORI_IMG = DIRECTORI_ESPORTS + DIR_SEPAR + "img";
    private static final String DIRECTORI_EQUIPS = DIRECTORI_ESPORTS + DIR_SEPAR + "equips";

    /* Quan llegeixo els fitxers, llegeixo directament tota la informacio del esport,
     * em vas comentar a classe que es millor llegir-ho un cop el usuari entra en un esport
     * per optimització de la memoria, pero com ja ho tenia fet aixi em vas dir que no em preocupes,
     * i ho deixes com ho tenia. */


    //Funcio que llegeix tots els esports del sistema de fitxers del dispositiu i retorna una collection amb els esports
    public static ArrayList<Esport> getEsports(Activity context){

        ArrayList<Esport> esports = new ArrayList<>();
        FileReader fr = null;
        BufferedReader br;
        FileReader frEquips;
        BufferedReader brEquips;

        try{
            fr = new FileReader(DIRECTORI_ESPORTS + DIR_SEPAR + "esports.txt");
            br = new BufferedReader(fr);

            String linia = br.readLine();


            while(linia != null){
                String[] lin = linia.split(":");
                String nom = lin[0];
                String file = lin[1];
                String img = DIRECTORI_IMG + DIR_SEPAR + file + ".png";


                //Ja em vas comentar que era mes optim no guardar els equips al club,
                //sino llegir-los al fer click a un esport, pero em vas dir que ho podia deixar aixi perque ja ho tenia fet
                ArrayList<String> clubs = new ArrayList<>();
                frEquips = new FileReader(DIRECTORI_EQUIPS + DIR_SEPAR + file + ".txt");
                brEquips = new BufferedReader(frEquips);

                String liniaEquip = brEquips.readLine();
                while(liniaEquip != null){
                    clubs.add(liniaEquip);
                    liniaEquip = brEquips.readLine();
                }

                String lliga = lin[2];
                String numClubs = lin[3];

                esports.add(new Esport(nom, lliga, file, Integer.parseInt(numClubs), img, clubs));
                linia = br.readLine();
            }

        }catch(FileNotFoundException ex){
            Toast.makeText(context, ex.toString(), Toast.LENGTH_LONG).show();
        }
        catch(IOException ex2){
            Toast.makeText(context, ex2.toString(), Toast.LENGTH_LONG).show();
        }finally {
            try{
                fr.close();
            }catch(IOException IOex) {
                Toast.makeText(context, IOex.toString(), Toast.LENGTH_LONG).show();
            }
        }
        return esports;
    }


}
