package com.example.cep.projectebuit;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Esport implements Parcelable {
    private String nom;
    private String lliga;
    private String file;
    private int numClubs;
    private String img;
    private ArrayList<String> clubs;



    public Esport(String nom, String lliga, String file, int numClubs, String img, ArrayList<String> clubs) {
        this.nom = nom;
        this.lliga = lliga;
        this.file = file;
        this.numClubs = numClubs;
        this.clubs = clubs;
        this.img = img;
    }

    public String getNom() {
        return nom;
    }

    public String getLliga() {
        return lliga;
    }

    public int getNumClubs() {
        return numClubs;
    }

    public String getImg() {
        return img;
    }

    public String getFile() {
        return file;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nom);
        dest.writeString(this.lliga);
        dest.writeString(this.file);
        dest.writeInt(this.numClubs);
        dest.writeString(this.img);
        dest.writeStringList(this.clubs);
    }

    protected Esport(Parcel in) {
        this.nom = in.readString();
        this.lliga = in.readString();
        this.file = in.readString();
        this.numClubs = in.readInt();
        this.img = in.readString();
        this.clubs = in.createStringArrayList();
    }

    public ArrayList<String> getClubs() {
        return clubs;
    }

    public static final Parcelable.Creator<Esport> CREATOR = new Parcelable.Creator<Esport>() {
        @Override
        public Esport createFromParcel(Parcel source) {
            return new Esport(source);
        }

        @Override
        public Esport[] newArray(int size) {
            return new Esport[size];
        }
    };
}
