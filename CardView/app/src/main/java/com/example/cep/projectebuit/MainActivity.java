package com.example.cep.projectebuit;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

	private static final int PERMISSION_REQUEST = 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/* Quan llegeixo els fitxers, llegeixo directament tota la informacio del esport,
		 * em vas comentar a classe que es millor llegir-ho un cop el usuari entra en un esport
		 * per optimització de la memoria, pero com ja ho tenia fet aixi em vas dir que no em preocupes,
		 * i ho deixes com ho tenia. */

		//DEMANEM PERMISOS

		if (android.os.Build.VERSION.SDK_INT >= 23)
		{
			// Si executem la versió Marshmallow (6.0) o posterior, haurem de demanar
			// permisos en temps d'execució

			// Comprovem si l'usuari ja ens ha donat permisos en una execió anterior
			if (ContextCompat.checkSelfPermission(this,
					Manifest.permission.READ_EXTERNAL_STORAGE)
					!= PackageManager.PERMISSION_GRANTED)
			{
				// Si l'usuari no ens havia atorgat permisos, els hi demanem i
				// executem el nostre codi
				ActivityCompat.requestPermissions(this,
						new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
						PERMISSION_REQUEST);
			}
			else
			{
				// Si l'usuari ja ens havia atorgat permisos en una execució anterior,
				// executem directament el nostre codi

				// Codi que volem executar
				app();
			}
		}
		else
		{
			// Si executem una versió anterior a la versió Marshmallow (6.0),
			// no cal demanar cap permís, i podem executar el nostre codi directament

			// Codi que volem executar
			app();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permission, int[] grantResults){
		switch (requestCode){
			case PERMISSION_REQUEST:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					app();
				}

		}
	}

	public void app(){
		final ArrayList<Esport> esports = EsportManager.getEsports(this);

		final EsportsAdapter adapter = new EsportsAdapter(esports);
		final RecyclerView recyclerView = findViewById(R.id.recyclerView);
		recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
		recyclerView.setAdapter(adapter);
		recyclerView.setHasFixedSize(true);



		adapter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				int i =recyclerView.getChildAdapterPosition(view);
				Esport esport = esports.get(i);

				Intent intent = new Intent(getApplicationContext(), Esport_activity.class);
				intent.putExtra("esport", esport);

				startActivity(intent);
				Toast.makeText(MainActivity.this, esport.getNom(), Toast.LENGTH_LONG).show();
			}
		});

	}
}
