package com.example.cep.projectebuit;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
{
	private CardView cardCaptura;
	private CardView cardReproduccio;
	private CardView cardEdicio;

	private static final int FILE_EXPLORER_FOR_REPRODUCCIO = 3;
	private static final int FILE_EXPLORER_FOR_EDICIO = 4;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


		cardCaptura = findViewById(R.id.cardCaptura);
		cardReproduccio = findViewById(R.id.cardReproduccio);
		cardEdicio = findViewById(R.id.cardEdicio);

		cardCaptura.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(MainActivity.this, CapturaActivity.class);
				startActivity(intent);
			}
		});

		cardReproduccio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFile(FILE_EXPLORER_FOR_REPRODUCCIO);
			}
		});

		cardEdicio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFile(FILE_EXPLORER_FOR_EDICIO);
			}
		});

	}


	public void getFile(int id){
		Intent fileIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
		fileIntent.addCategory(Intent.CATEGORY_OPENABLE);
		fileIntent.setType("*/*");
		startActivityForResult(fileIntent, id);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if(requestCode==FILE_EXPLORER_FOR_REPRODUCCIO && resultCode == RESULT_OK){

			Uri uri = data.getData();

			Intent i = new Intent(MainActivity.this, MediaActivity.class);
			i.putExtra("Uri", uri);
			startActivity(i);
		}else if(requestCode==FILE_EXPLORER_FOR_EDICIO && resultCode==RESULT_OK){

			Uri uri = data.getData();

			Intent i = new Intent(MainActivity.this, EditActivity.class);
			i.putExtra("Uri", uri);
			startActivity(i);
		}
	}




}
