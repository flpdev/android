package com.example.cep.projectebuit;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoViewAttacher;

public class MediaActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    //private String filePath;
    private ImageView imatge;
    private VideoView videoView;
    PhotoViewAttacher mAttacher;
    private LinearLayout imageLayout;
    private LinearLayout videoLayout;
    private LinearLayout centerVideo;
    private LinearLayout audioLayout;

    private ArrayList<String> formatsImatges = new ArrayList<>();
    private ArrayList<String> formatsVideos = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_media);

        imageLayout = findViewById(R.id.imageLayout);
        videoLayout= findViewById(R.id.videoLayout);
        audioLayout = findViewById(R.id.audioLayout);
        centerVideo = findViewById(R.id.centerVideo);

        imatge = findViewById(R.id.imatge);
        videoView = findViewById(R.id.videoView);

        generarFormats();

        String filePathString = (String)getIntent().getExtras().get("filePath");
        Uri filePath = Uri.parse(filePathString);
        String path = filePath.toString();

        String format = path.substring(path.lastIndexOf('.'));

        //Es una imatge
        if(formatsImatges.contains(format) || format.contains("image")){
            videoLayout.setVisibility(View.GONE);
            audioLayout.setVisibility(View.GONE);
            imatge.setImageURI(filePath);
            mAttacher = new PhotoViewAttacher(imatge);
            mAttacher.update();



        //Es un video
        }else if(format.contains("video") || formatsVideos.contains(format)){
            imageLayout.setVisibility(View.GONE);

            videoView.setVideoURI(filePath);

            videoView.setOnCompletionListener(this);


            MediaController mediaController = new
                    MediaController(this);
            mediaController.setAnchorView(videoView);
            videoView.setMediaController(mediaController);
            videoView.start();

        }else if(format.contains("audio")){

            try{
                imageLayout.setVisibility(View.GONE);

                //Per un fitxer d'audio podem utilitzar un videoView igualment
                videoView.setVideoURI(filePath);

                videoView.setOnCompletionListener(this);


                final CustomMediaController mediaController = new
                        CustomMediaController(this);
                mediaController.setAnchorView(centerVideo);

                videoView.setMediaController(mediaController);


                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        videoView.start();
                        mediaController.show();
                    }
                });

            }catch(Exception e){
                e.printStackTrace();
            }


        }else{
            Toast.makeText(this, "format del fitxer incorrecte", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        finish();
    }

    public void generarFormats(){
        //formats imatges
        formatsImatges.add(".jpg");
        formatsImatges.add(".jpeg");
        formatsImatges.add(".png");
        formatsImatges.add(".gif");
        formatsImatges.add(".bmp");

        //Video formats
        formatsVideos.add(".mp4");
        formatsVideos.add(".3gp");
    }
}
