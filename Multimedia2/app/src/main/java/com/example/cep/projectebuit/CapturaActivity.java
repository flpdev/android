package com.example.cep.projectebuit;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CapturaActivity extends AppCompatActivity {

    private static final int TAKE_PICTURE_REQUEST = 1;
    private static final int TAKE_VIDEO_REQUEST = 2;
    private String fotoPath;
    private String videoPath;

    private CardView cardCapturaFoto;
    private CardView cardCapturaVideo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captura);

        cardCapturaFoto = findViewById(R.id.cardCapturaFoto);
        cardCapturaVideo = findViewById(R.id.cardCapturaVideo);

        cardCapturaFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obrirCameraFoto();
            }
        });

        cardCapturaVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obrirCameraVideo();
            }
        });

    }

    private void obrirCameraFoto(){
        Intent ferFotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(ferFotoIntent.resolveActivity(getPackageManager()) != null){

            File fotoFile = null;

            try{
                fotoFile = createImageFile();
            }catch (IOException e){

            }


            if(fotoFile!=null){
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.cep.projectebuit.fileprovider",
                        fotoFile);
                ferFotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(ferFotoIntent, TAKE_PICTURE_REQUEST);
            }


        }
    }

    private void obrirCameraVideo(){
        Intent ferFotoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if(ferFotoIntent.resolveActivity(getPackageManager()) != null){

            File videoFile = null;

            try{
                videoFile = createVideoFile();
            }catch (IOException e){

            }


            if(videoFile!=null){
                Uri videoUri = FileProvider.getUriForFile(this,
                        "com.example.cep.projectebuit.fileprovider",
                        videoFile);
                ferFotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
                startActivityForResult(ferFotoIntent, TAKE_VIDEO_REQUEST);
            }


        }
    }



    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        fotoPath = image.getAbsolutePath();
        return image;
    }

    private File createVideoFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String videoFileName = "MP4_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File video = File.createTempFile(
                videoFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        videoPath = video.getAbsolutePath();
        return video;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode==TAKE_PICTURE_REQUEST && resultCode==RESULT_OK){

            Intent intent = new Intent(CapturaActivity.this, MediaActivity.class);
            intent.putExtra("filePath", fotoPath);
            startActivity(intent);
//
        }
        else if(requestCode == TAKE_VIDEO_REQUEST && resultCode == RESULT_OK){
            //VIDEO
            Intent intent = new Intent(CapturaActivity.this, MediaActivity.class);
            intent.putExtra("filePath", videoPath);
            startActivity(intent);
        }
    }
}
