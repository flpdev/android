package com.example.cep.projectebuit;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import uk.co.senab.photoview.PhotoViewAttacher;

public class EditActivity extends AppCompatActivity {

    private ArrayList<String> formatsImatges = new ArrayList<>();

    PhotoViewAttacher mAttacher;
    private ImageView img;
    private LinearLayout imageLayout;
    private Uri fileUri;
    private EditText alsada;
    private EditText amplada;
    private int imageHeight;
    private int imageWidth;
    private Button btnRedimensionar;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_edit);

        img = findViewById(R.id.imatgeEdit);
        amplada = findViewById(R.id.amplada);
        alsada = findViewById(R.id.altura);
        imageLayout = findViewById(R.id.imageLayout);
        btnRedimensionar = findViewById(R.id.btnResize);

        fileUri = (Uri) getIntent().getExtras().get("Uri");

        String path = fileUri.toString();

        String format = path.substring(path.lastIndexOf('.'));

        generarFormats();

        //Es una imatge
        if (format.contains("image") || formatsImatges.contains(format)) {

            bitmap = generarBitmapFromUri(fileUri);

            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);

                img.setImageBitmap(bitmap);
                img.setScaleType(ImageView.ScaleType.FIT_XY);

                //rotacio
//                Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotation);
//                img.startAnimation(rotation);

            } catch (IOException e) {
                e.printStackTrace();
            }

            //img.setImageURI(fileUri);

            imageHeight = bitmap.getHeight();
            imageWidth = bitmap.getWidth();


            alsada.setText(String.valueOf(imageHeight));
            amplada.setText(String.valueOf(imageWidth));

            //zoom
            mAttacher = new PhotoViewAttacher(img);
            mAttacher.update();
        }


        btnRedimensionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                redimensionarImatge(bitmap);
            }
        });
    }

    public void redimensionarImatge(Bitmap bitmap){


        int heigh = Integer.parseInt(alsada.getText().toString());
        int width = Integer.parseInt(amplada.getText().toString());

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, width, heigh , false);

        try{
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            FileOutputStream out = new FileOutputStream(image.getAbsolutePath());
            resizedBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);


            img.setImageBitmap(resizedBitmap);
//            Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotation);
//            img.startAnimation(rotation);

            //zoom
            mAttacher = new PhotoViewAttacher(img);
            mAttacher.update();

            imageHeight = resizedBitmap.getHeight();
            imageWidth = resizedBitmap.getWidth();

            //img.setImageBitmap(bitmap);
            alsada.setText(String.valueOf(imageHeight));
            amplada.setText(String.valueOf(imageWidth));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public Bitmap generarBitmapFromUri(Uri uri){
        Bitmap bitmap= null;
        try{
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        }catch (FileNotFoundException ex){
            ex.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

        return bitmap;
    }

    public void generarFormats(){
        //formats imatges
        formatsImatges.add(".jpg");
        formatsImatges.add(".jpeg");
        formatsImatges.add(".png");
        formatsImatges.add(".gif");
        formatsImatges.add(".bmp");
    }
}
