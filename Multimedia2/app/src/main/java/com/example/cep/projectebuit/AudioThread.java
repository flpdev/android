package com.example.cep.projectebuit;

import android.media.MediaPlayer;
import android.widget.TextView;

public class AudioThread extends Thread {

    private MediaPlayer mediaPlayer;
    private TextView currentSecond;
    private TextView audioDuration;



    public AudioThread(MediaPlayer mediaPlayer, TextView currentSecond, TextView audioDuration) {
        this.mediaPlayer = mediaPlayer;
        this.currentSecond = currentSecond;
        this.audioDuration = audioDuration;
    }

    @Override
    public void run() {
        mediaPlayer.start();
        while (mediaPlayer.isPlaying()){
            currentSecond.setText(""+mediaPlayer.getCurrentPosition() + " / ");
            audioDuration.setText(""+mediaPlayer.getDuration());
        }
    }
}
