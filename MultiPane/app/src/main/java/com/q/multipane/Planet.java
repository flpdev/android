package com.q.multipane;

import android.graphics.Bitmap;

public class Planet {

    private String name;
    private String image;
    private String descriptionFile;
    private Bitmap bitmap;

    public Planet(final String name,
                  final String image,
                  final String descriptionFile){
        this.name = name;
        this.image = image;
        this.descriptionFile = descriptionFile;
        this.bitmap = PlanetFileUtils.getBitmapFromFile(this.image);
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getDescriptionFile() {
        return descriptionFile;
    }

    public Bitmap getBitmap(){
        return this.bitmap;
    }
}
