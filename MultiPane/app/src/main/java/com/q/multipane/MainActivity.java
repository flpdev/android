package com.q.multipane;

import android.app.Activity;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.q.multipane.Adapters.PlanetAdapter;
import com.q.multipane.Fragments.DataFragment;
import com.q.multipane.Fragments.DetailFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements DataFragment.DataListener, DetailFragment.PlanetListener {

    private static final String TAG = MainActivity.class.getName();

    private boolean multiPanel = false;
    private DetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DataFragment dataFragment = new DataFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.linear, dataFragment).commit();

        if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            multiPanel = true;
            detailFragment = new DetailFragment();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.linear2, detailFragment).commit();
        }
    }

    @Override
    public void onClick(Planet planet) {
        //Send Planet to DetailFragment
        Log.d(TAG, "onClick() -> Planet: " + planet.getName());
        if(multiPanel){
            detailFragment.setValues(planet);
        } else {

        }
    }


    @Override
    public void onPlanetChanged() {

    }
}
