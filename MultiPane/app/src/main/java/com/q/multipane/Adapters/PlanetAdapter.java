package com.q.multipane.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.q.multipane.Planet;
import com.q.multipane.PlanetFileUtils;
import com.q.multipane.R;

import java.util.ArrayList;

public class PlanetAdapter extends RecyclerView.Adapter<PlanetAdapter.ViewHolder> implements View.OnClickListener{

    private static final String TAG = PlanetAdapter.class.getName();

    private ArrayList<Planet> planets;
    private View.OnClickListener listener;

    //Constructor
    public PlanetAdapter(final ArrayList<Planet> planets){
        this.planets = planets;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.data_list_item, viewGroup, false);
        //set listener
        itemView.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.image.setImageBitmap(planets.get(i).getBitmap());
        viewHolder.text.setText(planets.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return planets.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }

    }

    public Planet getPlanet(int position){
        return planets.get(position);
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        ImageView image;
        TextView text;

        public ViewHolder(View itemView){
            super(itemView);
            image = itemView.findViewById(R.id.image);
            text = itemView.findViewById(R.id.text);
        }

        public void bind(Planet planet){

        }
    }
}
