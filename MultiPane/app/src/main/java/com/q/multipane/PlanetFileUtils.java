package com.q.multipane;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PlanetFileUtils {
    private static final String TAG = PlanetFileUtils.class.getName();

    private static final String DIR_SEPARATOR = File.separator;
    private static final String DIRECTORY =  Environment.getExternalStorageDirectory()+ DIR_SEPARATOR + "planets";

    public static ArrayList<Planet> getPlanetsFromFiles(){
        ArrayList<Planet> planets =  new ArrayList<>();

        try{
            FileReader fileReader = new FileReader(DIRECTORY + DIR_SEPARATOR + "planets.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String linia = bufferedReader.readLine();

            while(linia != null){
                String name = linia.split(":")[0];
                String img = linia.split(":")[1];
                String descriptionFile = linia.split(":")[2];
                Planet p = new Planet(name, img, descriptionFile);
                planets.add(p);
                linia = bufferedReader.readLine();
            }
        }catch (Exception e){
            Log.d(TAG, "getPlanetsFromFiles " +  e.toString());
        }

        return planets;
    }

    public static String getPlanetDescription(Planet planet){

        try {
            FileReader fr = new FileReader(DIRECTORY + DIR_SEPARATOR + "descriptions" + DIR_SEPARATOR + planet.getDescriptionFile() + ".txt");
            BufferedReader br = new BufferedReader(fr);
            return br.readLine();
        } catch (IOException e) {
            Log.d(TAG, "getPlanetDescription " +  e.toString());
        }
        return "";
    }

    public static Bitmap getBitmapFromFile(String fileName){
        return BitmapFactory.decodeFile(DIRECTORY + DIR_SEPARATOR + "images" + DIR_SEPARATOR + fileName);
    }
}
