package com.q.multipane.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.q.multipane.Adapters.PlanetAdapter;
import com.q.multipane.Planet;
import com.q.multipane.PlanetFileUtils;
import com.q.multipane.R;

import java.util.ArrayList;

public class DataFragment extends Fragment {

    private DataListener callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.callback = (DataListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.data_fragment, container, false);
        ArrayList<Planet> planets = PlanetFileUtils.getPlanetsFromFiles();
        final RecyclerView recyclerView = view.findViewById(R.id.dataRecyclerView);
        final PlanetAdapter planetAdapter = new PlanetAdapter(planets);
        recyclerView.setAdapter(planetAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        planetAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Send data to detail fragment
                int position = recyclerView.getChildAdapterPosition(v);
                Planet p = planetAdapter.getPlanet(position);
                callback.onClick(p);
            }
        });

        return view;
    }

    public interface DataListener{
        void onClick(Planet planet);
    }
}
