package com.q.multipane.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.q.multipane.Planet;
import com.q.multipane.PlanetFileUtils;
import com.q.multipane.R;

public class DetailFragment extends Fragment {

    private TextView name;
    private ImageView imageView;
    private TextView desc;
    private PlanetListener planetListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        name = view.findViewById(R.id.planetName);
        imageView = view.findViewById(R.id.imageDetail);
        desc = view.findViewById(R.id.planetDesc);
        return view;
    }


   public void setValues(Planet planet){
        name.setText(planet.getName());
        imageView.setImageBitmap(planet.getBitmap());
        desc.setText(PlanetFileUtils.getPlanetDescription(planet));
   }

   public interface PlanetListener{
        void onPlanetChanged();
   }

}
