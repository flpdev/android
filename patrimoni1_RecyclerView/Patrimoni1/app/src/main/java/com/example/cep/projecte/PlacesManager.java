package com.example.cep.projecte;


import java.util.ArrayList;

public class PlacesManager {

    public static ArrayList<Place> getPlaces(){
        ArrayList<Place> places = new ArrayList<>();
        places.add(new Place("Casa vicens", R.drawable.casavicens));
        places.add(new Place("Colònia Guell", R.drawable.coloniaguell));
        places.add(new Place("Palau de la Música", R.drawable.palaumusica));
        places.add(new Place("Park Guell", R.drawable.parkguell));
        places.add(new Place("Tàrraco", R.drawable.tarraco));
        places.add(new Place("Vall de Boí", R.drawable.valldeboi));

        return places;
    }

    public static ExtendedPlace getPlace(Place place)
    {
        String author = null, style = null, municipality = null, heritageCode = null;
        int endYear = 0, heritageYear = 0;
        Coordinates coord = null;
        String placeName = place.getName();

        switch(placeName)
        {
            case "Casa vicens":
                author = "Antoni Gaudí";
                endYear = 1888;
                style = "Modernisme";
                coord = new Coordinates(41.403611111111,2.1511111111111);
                municipality = "Barcelona";
                heritageYear = 1984;
                heritageCode = "320-004";
                break;
            case "Colònia Guell":
                author = "Antoni Gaudí";
                endYear = 1915;
                style = "Modernisme";
                coord = new Coordinates(41.36358333,2.02791667);
                municipality = "Santa Coloma de Cervelló";
                heritageYear = 1984;
                heritageCode = "320-007";
                break;
            case "Palau de la Música":
                author = "Antoni Gaudí";
                endYear = 1905;
                style = "Modernisme";
                coord = new Coordinates(41.387666666667,2.1752777777778);
                municipality = "Barcelona";
                heritageYear = 1997;
                heritageCode = "804bis-001";
                break;
            case "Park Guell":
                author = "Antoni Gaudí";
                endYear = 1888;
                style = "Modernisme";
                coord = new Coordinates(41.403611111111,2.1511111111111);
                municipality = "Barcelona";
                heritageYear = 1984;
                heritageCode = "320-001";
                break;
            case "Tàrraco":
                author = "Diversos";
                endYear = -200;
                style = "Romà";
                coord = new Coordinates(41.11472222,1.259305556);
                municipality = "Tarragona";
                heritageYear = 2000;
                heritageCode = "875";
                break;
            case "Vall de Boí":
                author = "Diversos";
                endYear = 1150;
                style = "Romànic";
                coord = new Coordinates(42.50472222,0.80361111);
                municipality = "La Vall de Boí";
                heritageYear = 2000;
                heritageCode = "988";
                break;
        }

        ExtendedPlace extPlace = new ExtendedPlace(place, author, endYear, style, coord, municipality, heritageYear, heritageCode);

        return extPlace;
    }


}
