package com.example.cep.projecte;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Place{

    protected String name;
    protected int img;

    protected Place(Parcel in) {
        name = in.readString();
        img = in.readInt();
    }

    public Place() {
    }

    public Place(String name, int img) {
        this.name = name;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public int getImg() {
        return img;
    }

}
