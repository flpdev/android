package com.example.cep.projecte;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.Serializable;

public class  PlaceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);

        //GET CLASS
        final ExtendedPlace extPlace = getIntent().getParcelableExtra("extPlace");
//        Place place = getIntent().getParcelableExtra("place");


        //INFO DEL PATRIMONI

        TextView nom = findViewById(R.id.txtTitol);
        nom.setText(extPlace.getName());

        ImageView img = findViewById(R.id.img);
        Bitmap bitmap = BitmapFactory.decodeFile(extPlace.getImg());
        img.setImageBitmap(bitmap);

        TextView autor = findViewById(R.id.txtAutor);
        autor.setText(extPlace.getAuthor());

        TextView anyFinalitzacio = findViewById(R.id.txtAny);
        anyFinalitzacio.setText(String.valueOf(extPlace.getEndYear()));

        TextView estil = findViewById(R.id.txtEstil);
        estil.setText(extPlace.getStyle());

        TextView coordenades = findViewById(R.id.txtCoordenades);
        coordenades.setText(String.valueOf(extPlace.getCoordinates().getLongitud()).concat(", ").concat(String.valueOf(extPlace.getCoordinates().getLatitud())));

        TextView municipi = findViewById(R.id.txtMunicipi);
        municipi.setText(extPlace.getMunicipality());

        TextView anyDeclaracio = findViewById(R.id.anyDeclaracio);
        anyDeclaracio.setText(String.valueOf(extPlace.getHeritageYear()));

        TextView codiDeclaracio = findViewById(R.id.txtCodiDeclaracio);
        codiDeclaracio.setText(extPlace.getHeritageCode());






        //BUTTONS
        Button btnCancelar = findViewById(R.id.btnCancelar);
        Button btnDesar = findViewById(R.id.btnDesar);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnDesar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PlaceActivity.this, MainActivity.class);
                intent.putExtra("extPlaceNom", extPlace.getName());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

    }
}
