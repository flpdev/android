
package com.example.cep.projecte;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.nio.InvalidMarkException;
import java.util.ArrayList;
import java.util.List;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder> implements View.OnClickListener {
    private Context _context;
    private List<Place> places;
    private View.OnClickListener listener;

    public PlaceAdapter(ArrayList<Place> places) {
        this.places = places;
    }



    @NonNull
    @Override
    public PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.place_item, viewGroup, false);
        itemView.setOnClickListener(this);
        PlaceViewHolder PlaceViewHolder = new PlaceViewHolder(itemView);
        return PlaceViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceViewHolder placeViewHolder, int i) {
        placeViewHolder.bindPlace(places.get(i));
    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener!= null){
            listener.onClick(view);
        }

    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView txtView;
        public PlaceViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ImgMenuPlace);
            txtView = itemView.findViewById(R.id.txtMenuPlanetName);
        }

        public void bindPlace(Place place){
            txtView.setText(place.getName());
            Bitmap bitmap = BitmapFactory.decodeFile(place.getImg());
            imageView.setImageBitmap(bitmap);
        }
    }

}
