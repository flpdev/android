package com.example.cep.projecte;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class ExtendedPlace extends Place implements Parcelable {
    private String author;
    private int endYear;
    private String style;
    private Coordinates coordinates;
    private String municipality;
    private int heritageYear;
    private String heritageCode;

    public ExtendedPlace(Place place, String author, int endYear, String style, Coordinates coordinates, String municipality, int heritageYear, String heritageCode) {
        super(place.getName(), place.getImg(), place.getFile());
        this.author = author;
        this.endYear = endYear;
        this.style = style;
        this.coordinates = coordinates;
        this.municipality = municipality;
        this.heritageYear = heritageYear;
        this.heritageCode = heritageCode;
    }

    protected ExtendedPlace(Parcel in) {
        name = in.readString();
        img = in.readString();
        author = in.readString();
        endYear = in.readInt();
        style = in.readString();
        coordinates = (Coordinates) in.readValue(Coordinates.class.getClassLoader());
        municipality = in.readString();
        heritageYear = in.readInt();
        heritageCode = in.readString();
    }


    public String getAuthor() {
        return author;
    }

    public int getEndYear() {
        return endYear;
    }

    public String getStyle(){ return style;}

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public String getMunicipality() {
        return municipality;
    }

    public int getHeritageYear() {
        return heritageYear;
    }

    public String getHeritageCode() {
        return heritageCode;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(img);
        dest.writeString(author);
        dest.writeInt(endYear);
        dest.writeString(style);
        dest.writeValue(coordinates);
        dest.writeString(municipality);
        dest.writeInt(heritageYear);
        dest.writeString(heritageCode);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ExtendedPlace> CREATOR = new Parcelable.Creator<ExtendedPlace>() {
        @Override
        public ExtendedPlace createFromParcel(Parcel in) {
            return new ExtendedPlace(in);
        }

        @Override
        public ExtendedPlace[] newArray(int size) {
            return new ExtendedPlace[size];
        }
    };
}
