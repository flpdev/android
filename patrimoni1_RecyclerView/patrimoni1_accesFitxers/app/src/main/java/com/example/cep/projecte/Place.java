package com.example.cep.projecte;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Place{

    protected String name;
    protected String img;
    protected String file;

    protected Place(Parcel in) {
        name = in.readString();
        img = in.readString();
    }

    public Place() {
    }

    public Place(String name, String img, String file) {
        this.name = name;
        this.img = img;
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public String getImg() {
        return img;
    }

    public String getFile(){ return file; }

}
