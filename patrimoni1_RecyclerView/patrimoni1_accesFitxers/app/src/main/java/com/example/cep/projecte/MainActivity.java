package com.example.cep.projecte;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

public class MainActivity extends AppCompatActivity{

	static final int PERMISSION_REQUEST = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Controlem la versió d'Android que estem executant.
		if (android.os.Build.VERSION.SDK_INT >= 23)
		{
			// Si executem la versió Marshmallow (6.0) o posterior, haurem de demanar
			// permisos en temps d'execució

			// Comprovem si l'usuari ja ens ha donat permisos en una execió anterior
			if (ContextCompat.checkSelfPermission(this,
					Manifest.permission.READ_EXTERNAL_STORAGE)
					!= PackageManager.PERMISSION_GRANTED)
			{

				// Si l'usuari no ens havia atorgat permisos, els hi demanem i
				// executem el nostre codi

				ActivityCompat.requestPermissions(this,
						new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
						PERMISSION_REQUEST);

				// Codi que volem executar

			}
			else
			{
				// Si l'usuari ja ens havia atorgat permisos en una execució anterior,
				// executem directament el nostre codi

				// Codi que volem executar
				patrimoni();
			}
		}
		else
		{

			// Si executem una versió anterior a la versió Marshmallow (6.0),
			// no cal demanar cap permís, i podem executar el nostre codi directament

			// Codi que volem executar
			patrimoni();
		}



	}

	//funcio principal del programa que s'executara quan la aplicacio tingui permisos de lectura del storage
	public void patrimoni(){
		final TextView txtSelectedPlace = findViewById(R.id.txtSelectedPlace);
		final int PLACE_ACTIVITY = 1;

		final GridView gridView = findViewById(R.id.grdMenu);
		final TextView txtView = findViewById(R.id.txtSelectedPlace);

		PlacesManager placesManager = new PlacesManager();
		final List<Place> listPatrimonis = placesManager.getPlaces(this);
		PlaceAdapter plcAdapter = new PlaceAdapter(this, R.layout.place_item, listPatrimonis);
		gridView.setAdapter(plcAdapter);

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

				Intent intent = new Intent(getApplicationContext(), PlaceActivity.class);
				intent.putExtra("extPlace", PlacesManager.getPlace(listPatrimonis.get(i), MainActivity.this));

				startActivityForResult(intent, PLACE_ACTIVITY);
			}
		});
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent){
		if(requestCode==1){
			if(resultCode==RESULT_OK){
				final TextView txtSelectedPlace = findViewById(R.id.txtSelectedPlace);
				txtSelectedPlace.setText((String)intent.getExtras().getString("extPlaceNom"));
			}
		}
	}

	//Despres de demanar permisos controlem si han estat donats o no i cridem la funcion principal patrimoni.
	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case PERMISSION_REQUEST: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					patrimoni();
				}
			}
		}
	}
}
