package com.example.cep.projecte;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.nio.InvalidMarkException;
import java.util.ArrayList;
import java.util.List;

public class PlaceAdapter extends ArrayAdapter {
    private Context _context;
    private List<Place> places = new ArrayList<>();

    public PlaceAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this._context = context;
        this.places = objects;

    }


    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        listItem = LayoutInflater.from(_context).inflate(R.layout.place_item, parent, false);

        Place currentPlace = places.get(position);

        Bitmap bitmap = BitmapFactory.decodeFile(currentPlace.getImg());

        ImageView image = listItem.findViewById(R.id.ImgMenuPlace);
        image.setImageBitmap(bitmap);

        TextView name = listItem.findViewById(R.id.txtMenuPlanetName);
        name.setText(currentPlace.getName());

        return listItem;

    }
}
