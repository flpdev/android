package com.example.cep.projecte;


import android.app.Activity;
import android.os.Environment;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class PlacesManager {

    public static final String DIR_SEPAR = File.separator;
    public static final String DIRECTORY_PATRIMONI = Environment.getExternalStorageDirectory() +
            DIR_SEPAR + "Patrimoni";
    public static final String FILES_DIR = DIRECTORY_PATRIMONI + DIR_SEPAR + "files";
    public static final String IMAGES_DIR = DIRECTORY_PATRIMONI + DIR_SEPAR + "img";


    public static ArrayList<Place> getPlaces(Activity context)
    {
        ArrayList<Place> places = new ArrayList<>();

        try
        {
            FileReader fr = new FileReader(DIRECTORY_PATRIMONI + DIR_SEPAR + "patrimoni.txt");
            BufferedReader br = new BufferedReader(fr);

            String linia = br.readLine();
            while(linia != null)
            {
                String [] lin = linia.split(":");
                String name = lin[0];
                String arx  = lin[1];
                String img  = IMAGES_DIR + DIR_SEPAR + arx + ".jpg";
                String file = FILES_DIR + DIR_SEPAR + arx + ".txt";
                places.add(new Place(name, img, file));
                linia = br.readLine();
            }

            fr.close();
        }
        catch(Exception e)
        {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }

        return places;
    }


    public static ExtendedPlace getPlace(Place place, Activity context)
    {
        ExtendedPlace extPlace = null;

        try
        {
            FileReader fr = new FileReader(place.getFile());
            BufferedReader br = new BufferedReader(fr);

            String author = br.readLine().split(":")[1];
            int endYear = Integer.parseInt(br.readLine().split(":")[1]);
            String style = br.readLine().split(":")[1];
            String [] coordinates = (br.readLine().split(":")[1]).split(",");
            double lat = Double.parseDouble(coordinates[0]);
            double lon = Double.parseDouble(coordinates[1]);
            Coordinates coord = new Coordinates(lat, lon);
            String municipality = br.readLine().split(":")[1];
            int heritageYear = Integer.parseInt(br.readLine().split(":")[1]);
            String heritageCode = br.readLine().split(":")[1];

            extPlace = new ExtendedPlace(place,
                    author,
                    endYear,
                    style,
                    coord,
                    municipality,
                    heritageYear,
                    heritageCode);

            fr.close();
        }
        catch(Exception e)
        {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }

        return extPlace;
    }



}
