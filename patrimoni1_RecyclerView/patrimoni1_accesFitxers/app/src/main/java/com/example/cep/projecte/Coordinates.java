package com.example.cep.projecte;

import android.os.Parcel;
import android.os.Parcelable;

import java.security.PrivilegedActionException;

public class Coordinates implements Parcelable {
    private double longitud;
    private double latitud;

    public Coordinates(double longitud, double latitud) {
        this.longitud = longitud;
        this.latitud = latitud;
    }

    protected Coordinates(Parcel in) {
        longitud = in.readDouble();
        latitud = in.readDouble();
    }

    public static final Creator<Coordinates> CREATOR = new Creator<Coordinates>() {
        @Override
        public Coordinates createFromParcel(Parcel in) {
            return new Coordinates(in);
        }

        @Override
        public Coordinates[] newArray(int size) {
            return new Coordinates[size];
        }
    };

    public double getLongitud() {
        return longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(longitud);
        parcel.writeDouble(latitud);
    }
}
