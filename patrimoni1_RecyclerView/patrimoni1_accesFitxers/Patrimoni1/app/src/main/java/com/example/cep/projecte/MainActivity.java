package com.example.cep.projecte;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

public class MainActivity extends AppCompatActivity{


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final TextView txtSelectedPlace = findViewById(R.id.txtSelectedPlace);
		final int PLACE_ACTIVITY = 1;

		final GridView gridView = findViewById(R.id.grdMenu);
		final TextView txtView = findViewById(R.id.txtSelectedPlace);

		PlacesManager placesManager = new PlacesManager();
		final List<Place> listPatrimonis = placesManager.getPlaces();
		PlaceAdapter plcAdapter = new PlaceAdapter(this, R.layout.place_item, listPatrimonis);
		gridView.setAdapter(plcAdapter);

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

				Intent intent = new Intent(getApplicationContext(), PlaceActivity.class);
				intent.putExtra("extPlace", PlacesManager.getPlace(listPatrimonis.get(i)));

				startActivityForResult(intent, PLACE_ACTIVITY);
			}
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent){
		if(requestCode==1){
			if(resultCode==RESULT_OK){
				final TextView txtSelectedPlace = findViewById(R.id.txtSelectedPlace);
				txtSelectedPlace.setText((String)intent.getExtras().getString("extPlaceNom"));
			}
		}
	}
}
